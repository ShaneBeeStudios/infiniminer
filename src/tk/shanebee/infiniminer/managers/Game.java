package tk.shanebee.infiniminer.managers;

import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import tk.shanebee.infiniminer.classes.Class;

import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class Game {

	public String name;
	private Bound bound;
	private int timer;
	private int minPlayers;
	private int maxPlayers;
	private Status status;

	public Map<Player, Class> players;

	// LOAD GAME FROM CONFIG
	public Game(String name, Bound bound, List<Location> spawns, int timer, int min, int max, boolean isReady) {
		this.name = name;
		this.bound = bound;
		this.timer = timer;
		this.minPlayers = min;
		this.maxPlayers = max;

		if (isReady) this.status = Status.READY;
		else this.status = Status.BROKEN;
	}

	// NEW GAME
	public Game(String name, Bound bound, int timer, int min, int max) {
		this.name = name;
		this.bound = bound;
		this.timer = timer;
		this.minPlayers = min;
		this.maxPlayers = max;
		this.status = Status.NOTREADY;
	}

	/** Get the minimum amount of players required to start this game
	 * @return Min players
	 */
	public int getMinPlayers() {
		return this.minPlayers;
	}

	/** Get the maximum players allowed to play this game
	 * @return Max players
	 */
	public int getMaxPlayers() {
		return this.maxPlayers;
	}

	/** Get the current status of this game
	 * @return The status of this game
	 */
	public Status getStatus() {
		return this.status;
	}

	/** Get the timer for this game
	 * @return The timer for this game (in seconds)
	 */
	public int getTimer() {
		return this.timer;
	}

	/** Get the bound for this game
	 * @return The bound for this game
	 */
	public Bound getBound() {
		return this.bound;
	}

	/** Get the players of this game
	 * @return A map with players and their classes
	 */
	public Map<Player, Class> getPlayers() {
		return this.players;
	}

	/** Join a player to this game
	 * @param player The player to join
	 * @param gameClass The class of the player (Can be null)
	 */
	public void joinGame(Player player, @Nullable Class gameClass) {
		players.put(player, gameClass);
	}

	/** Remove a player from this game
	 * @param player The player to remove
	 */
	public void leaveGame(Player player) {
		players.remove(player);
	}

	/** Change the class of a player for this game
	 * @param player The player to change a class for
	 * @param gameClass The new class
	 */
	public void changeClass(Player player, Class gameClass) {
		if (players.containsKey(player)) {
			players.replace(player, gameClass);
		}
	}

}
