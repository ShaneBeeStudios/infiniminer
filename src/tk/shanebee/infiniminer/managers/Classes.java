package tk.shanebee.infiniminer.managers;

public enum Classes {

	MINER("Miner"),
	PROSPECTOR("Prospector"),
	ENGINEER("Engineer"),
	SAPPER("Sapper	");

	private String name;

	Classes(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
