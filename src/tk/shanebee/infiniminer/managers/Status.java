package tk.shanebee.infiniminer.managers;

import tk.shanebee.infiniminer.utils.Utils;

public enum Status {

	RUNNING(Utils.coloredString("&aRUNNING")),
	WAITING(Utils.coloredString("&6WAITING")),
	STOPPED(Utils.coloredString("&cSTOPPED")),
	READY(Utils.coloredString("&aREADY")),
	NOTREADY(Utils.coloredString("&6NOT READY")),
	BROKEN(Utils.coloredString("&cBROKEN"));

	private String name;

	Status(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
