package tk.shanebee.infiniminer.classes;

import org.bukkit.Material;

import java.util.Arrays;

public class Sapper extends Class {

	public Sapper() {
		super.name = "Sapper";
		super.weight = 4;
		super.canCarry = 200;
		super.canBuild = Arrays.asList(Material.STONE, Material.DIRT);
	}

}
