package tk.shanebee.infiniminer.classes;

import org.bukkit.Material;

import java.util.List;

@SuppressWarnings("unused")
public abstract class Class {

	String name;
	int canCarry;
	int weight;
	List<Material> canBuild;

	public String getName() {
		return name;
	}

	/** Get the amount of blocks this class can carry
	 * @return Amount of blocks
	 */
	public int getCanCarry() {
		return canCarry;
	}

	public int getWeight() {
		return weight;
	}

	/** Get the list of materials this class can build with
	 * @return List of materials
	 */
	public List<Material> getCanBuild() {
		return canBuild;
	}

	/** Check if this class can carry a certain material
	 * @param material The material to check
	 * @return True if this class can carry said material
	 */
	public boolean canBuild(Material material) {
		return canBuild.contains(material);
	}

}
