package tk.shanebee.infiniminer.classes;

import org.bukkit.Material;

import java.util.Arrays;

public class Prospector extends Class {

	public Prospector() {
		super.name = "Prospector";
		super.weight = 4;
		super.canCarry = 200;
		super.canBuild = Arrays.asList(Material.STONE, Material.DIRT);
	}
}
