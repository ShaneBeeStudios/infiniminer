package tk.shanebee.infiniminer.classes;

import org.bukkit.Material;

import java.util.Arrays;

public class Miner extends Class {

	public Miner() {
		super.name = "Miner";
		super.weight = 8;
		super.canCarry = 200;
		super.canBuild = Arrays.asList(Material.STONE, Material.DIRT);
	}

}
