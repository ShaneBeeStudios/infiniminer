package tk.shanebee.infiniminer.classes;

import org.bukkit.Material;

import java.util.Arrays;

public class Engineer extends Class {

	public Engineer() {
		super.name = "Engineer";
		super.weight = 4;
		super.canCarry = 350;
		super.canBuild = Arrays.asList(Material.STONE, Material.DIRT);
	}

}
