package tk.shanebee.infiniminer.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.configs.GamesConfig;
import tk.shanebee.infiniminer.generators.IslandGenerator;
import tk.shanebee.infiniminer.managers.Bound;
import tk.shanebee.infiniminer.managers.Game;
import tk.shanebee.infiniminer.utils.Utils;

public class CreateCmd extends BaseCmd {

	public CreateCmd() {
		forcePlayer = true;
		cmdName = "create";
		argLength = 4;
		usage = "<name> <timer> <min-player> <max-player>";
	}

	private final IslandGenerator islandGenerator = new IslandGenerator(Infiniminer.plugin);
	private final GamesConfig gamesConfig = Infiniminer.gamesConfig;

	@Override
	public boolean run() {
		String name = args[1];
		int timer = Integer.valueOf(args[2]);
		int min = Integer.valueOf(args[3]);
		int max = Integer.valueOf(args[4]);
		if (!gamesConfig.games.isSet("Games." + name + ".info.timer")) {
			createGame(name, timer, min, max);
		} else {
			Utils.sendColMessage(sender, "&cGame &a" + name + "&c is already taken");
		}
		return true;
	}

	private Location nextGame() {
		int nextGameConfig = Infiniminer.gamesConfig.nextGame;
		int nextSize = 128;
		int nextGame = (nextGameConfig * nextSize) + 100;
		return new Location(Bukkit.getWorld("world_infiniminer"), 1.0, 0.0, ((double) nextGame));
	}

	private void createGame(String name, int timer, int min, int max) {
		Location loc1 = nextGame();
		Location loc2 = nextGame().add(64, 128, 64);
		Infiniminer.gamesConfig.updateNextGame();

		Bound bound = new Bound(loc1, loc2);
		Game game = new Game(name, bound, timer, min, max);

		islandGenerator.createIsland(loc1, bound);
		Infiniminer.plugin.games.add(game);
		gamesConfig.createGamesConfig(name, timer, min, max, bound);
	}

}
