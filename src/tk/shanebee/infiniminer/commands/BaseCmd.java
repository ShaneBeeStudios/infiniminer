package tk.shanebee.infiniminer.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.utils.Utils;

public abstract class BaseCmd {

	public CommandSender sender;
	public String[] args;
	String cmdName;
	int argLength = 0;
	boolean forcePlayer = true;
	public boolean forceInGame = false;
	Player player;
	String usage = "";
	Infiniminer plugin;

	boolean processCmd(Infiniminer plugin, CommandSender sender, String[] arg) {
		this.sender = sender;
		this.args = arg;
		this.plugin = plugin;

		if (forcePlayer) {
			if (!(sender instanceof Player)) return false;
			else this.player = (Player) sender;
		}
		if (!sender.hasPermission("infiniminer." + cmdName))
			Utils.sendColMessage(sender, "&cYou do not have permission to run this command");
		else return run();
		return true;
	}

	public abstract boolean run();

	String sendHelpLine() {
		return "&3&l/infiniminer &b" + cmdName + " &6" + usage.replaceAll("<", "&7&l<&f").replaceAll(">", "&7&l>");
	}

}
