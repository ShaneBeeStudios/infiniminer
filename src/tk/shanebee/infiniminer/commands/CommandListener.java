package tk.shanebee.infiniminer.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;
import tk.shanebee.infiniminer.Infiniminer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("NullableProblems")
public class CommandListener implements CommandExecutor, TabCompleter {

	private final Infiniminer plugin;

	public CommandListener(Infiniminer plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0 || !plugin.cmds.containsKey(args[0])) {
			sender.sendMessage(""); // TODO put stuff here
		} else {
			plugin.cmds.get(args[0]).processCmd(plugin, sender, args);
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String s, String[] args) {
		if (args.length == 1) {
			ArrayList<String> matches = new ArrayList<>();
			for (String name : plugin.cmds.keySet()) {
				if (StringUtil.startsWithIgnoreCase(name, args[0])) {
					if (sender.hasPermission("infiniminer." + name))
						matches.add(name);
				}
			}
			return matches;
		}
		return Collections.emptyList();
	}

}
