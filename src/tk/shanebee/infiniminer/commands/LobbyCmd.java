package tk.shanebee.infiniminer.commands;

public class LobbyCmd extends BaseCmd {

	public LobbyCmd() {
		cmdName = "lobby";
	}

	@Override
	public boolean run() {
		player.teleport(plugin.worldGenerator.spawn);
		return true;
	}

}
