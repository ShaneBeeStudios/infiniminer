package tk.shanebee.infiniminer.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.shanebee.infiniminer.Infiniminer;

public class Utils {

    /** Send a colored message to console
     * @param message Message to send
     */
    public static void sendColConsole(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /** Send a colored message to console including prefix
     * @param message Message to send
     */
    public static void sendConsoleLog(String message) {
        String prefix = Infiniminer.lang.prefix;
        //String prefix = "&7[&3Infiniminer&7]";
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " " + message));
    }

    /** Send a colored message to a player
     * @param player Player to send message to
     * @param message Message to send
     */
    public static void sendColMessage(CommandSender player, String message) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    /** Get a formatted colored string
     * @param string String with color codes
     * @return A formatted colored string
     */
    public static String coloredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

}
