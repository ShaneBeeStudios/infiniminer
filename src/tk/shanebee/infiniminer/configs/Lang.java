package tk.shanebee.infiniminer.configs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.utils.Utils;

import java.io.File;

public class Lang {

	public String prefix;

	public Lang(Infiniminer plugin) {
		loadLangFile(plugin);
		loadLang();
	}

	private void loadLangFile(Infiniminer main) {
		String loaded;
		File lang_file = new File(main.getDataFolder(), "lang.yml");
		if (!lang_file.exists()) {
			main.saveResource("lang.yml", true);
			loaded = "&aNew lang.yml created";
		} else {
			loaded = "&7 lang.yml &aloaded";
		}
		FileConfiguration lang = YamlConfiguration.loadConfiguration(lang_file);
		prefix = lang.getString("prefix");
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + loaded));
	}

	private void loadLang() {
	}

}
