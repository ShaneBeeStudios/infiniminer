package tk.shanebee.infiniminer.configs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.managers.Bound;

import java.io.File;
import java.io.IOException;

public class GamesConfig {

	public FileConfiguration games;
	private File games_file;
	private Infiniminer plugin;

	public int nextGame;

	public GamesConfig(Infiniminer plugin) {
		this.plugin = plugin;
		loadGamesFile();
		loadGamesConfig();
	}

	private void loadGamesFile() {
		String loaded;
		games_file = new File(plugin.getDataFolder(), "games.yml");
		if (!games_file.exists()) {
			plugin.saveResource("games.yml", true);
			loaded = "&aNew games.yml created";
		} else {
			loaded = "&7games.yml loaded";
		}
		games = YamlConfiguration.loadConfiguration(games_file);
		String prefix = Infiniminer.lang.prefix;
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + loaded));
	}

	private void loadGamesConfig() {
		nextGame = games.getInt("next-game");
	}

	private void saveConfig() {
		try {
			games.save(games_file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateNextGame() {
		loadGamesConfig();
		games.set("next-game", nextGame + 1);
		saveConfig();
	}

	public void createGamesConfig(String name, int timer, int min, int max, Bound bound) {
		games.set("Games." + name + ".bound.x", bound.getLesserCorner().getX());
		games.set("Games." + name + ".bound.y", bound.getLesserCorner().getY());
		games.set("Games." + name + ".bound.z", bound.getLesserCorner().getZ());
		games.set("Games." + name + ".bound.x2", bound.getGreaterCorner().getX());
		games.set("Games." + name + ".bound.y2", bound.getGreaterCorner().getY());
		games.set("Games." + name + ".bound.z2", bound.getGreaterCorner().getZ());
		games.set("Games." + name + ".info.timer", timer);
		games.set("Games." + name + ".info.min-players", min);
		games.set("Games." + name + ".info.max-players", max);
		saveConfig();
	}

}
