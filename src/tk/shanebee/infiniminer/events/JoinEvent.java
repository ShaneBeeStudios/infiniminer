package tk.shanebee.infiniminer.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.shanebee.infiniminer.Infiniminer;

public class JoinEvent implements Listener {

	private Infiniminer plugin;

	public JoinEvent(Infiniminer plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	private void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			player.sendMessage("Teleporting to the island");
			player.teleport(Infiniminer.plugin.worldGenerator.spawn);
		}, 1);
	}

}
