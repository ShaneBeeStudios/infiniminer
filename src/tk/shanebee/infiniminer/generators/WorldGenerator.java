package tk.shanebee.infiniminer.generators;

import org.bukkit.*;
import org.bukkit.block.Biome;
import org.bukkit.generator.ChunkGenerator;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.utils.BlockIterator;

import java.util.Random;

public class WorldGenerator {

	private BlockIterator blockIterator;
	public Location spawn;
	private World world;

	public WorldGenerator() {
		blockIterator = Infiniminer.plugin.blockIterator;
		loadWorld();
	}

	private void loadWorld() {
		if (Bukkit.getWorld("world_infiniminer") == null) {
			WorldCreator world = new WorldCreator("world_infiniminer");
			world.type(WorldType.FLAT);
			world.generator(new ChunkGenerator() {
				@SuppressWarnings("NullableProblems")
				@Override
				public ChunkData generateChunkData(World world, Random random, int cx, int cz, ChunkGenerator.BiomeGrid biome) {
					ChunkData chunkData = createChunkData(world);
					for (int x = 0; x <= 15; x++) {
						for (int z = 0; z <= 15; z++) {
							biome.setBiome(x, z, Biome.PLAINS);
						}
					}
					return chunkData;
				}
			});
			world.createWorld();
		}
		world = Bukkit.getWorld("world_infiniminer");
		spawn = new Location(world, 1, 65, 1);
		setSpawn();
	}

	private void setSpawn() {
		if (world.getBlockAt(1, 64, 1).getType() != Material.AIR) return;
		Location loc1 = new Location(world, 10, 64, 10);
		Location loc2 = new Location(world, -10, 64, -10);
		blockIterator.setBlocks(loc1, loc2, Material.GRASS_BLOCK);
		assert world != null;
		world.setSpawnLocation(spawn);
		world.generateTree(new Location(world, 5, 65, 5), TreeType.TREE);
	}

}
