package tk.shanebee.infiniminer.generators;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import tk.shanebee.infiniminer.Infiniminer;
import tk.shanebee.infiniminer.managers.Bound;

public class IslandGenerator {

	private Infiniminer plugin;

	public IslandGenerator(Infiniminer plugin) {
		this.plugin = plugin;
	}

	public void createIsland(Location location, Bound bound) {
		for (Block block : bound.getAllBlocks()) {
			if (block.getLocation().getY() == 64 ) {
				block.setType(Material.GRASS_BLOCK);
			}
			if (block.getLocation().getY() <= 63) {
				block.setType(Material.STONE);
			}
		}
	}

}
