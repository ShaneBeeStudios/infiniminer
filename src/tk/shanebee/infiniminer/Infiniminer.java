package tk.shanebee.infiniminer;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;
import tk.shanebee.infiniminer.commands.BaseCmd;
import tk.shanebee.infiniminer.commands.CommandListener;
import tk.shanebee.infiniminer.commands.CreateCmd;
import tk.shanebee.infiniminer.commands.LobbyCmd;
import tk.shanebee.infiniminer.configs.Config;
import tk.shanebee.infiniminer.configs.GamesConfig;
import tk.shanebee.infiniminer.configs.Lang;
import tk.shanebee.infiniminer.events.JoinEvent;
import tk.shanebee.infiniminer.generators.IslandGenerator;
import tk.shanebee.infiniminer.generators.WorldGenerator;
import tk.shanebee.infiniminer.managers.Game;
import tk.shanebee.infiniminer.utils.BlockIterator;
import tk.shanebee.infiniminer.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Infiniminer extends JavaPlugin {

	public static Infiniminer plugin;
	public static Config config;
	public static Lang lang;
	public static GamesConfig gamesConfig;

	public BlockIterator blockIterator;
	public IslandGenerator islandGenerator;
	public WorldGenerator worldGenerator;

	public HashMap<String, BaseCmd> cmds = new HashMap<>();
	public List<Game> games = new ArrayList<>();


	/**
	 * Internal use only
	 */
	@Override
	public void onEnable() {
		plugin = this;
		config = new Config(this);
		lang = new Lang(this);
		gamesConfig = new GamesConfig(this);

		loadCommands();
		loadEvents();

		// BlockIterator
		blockIterator = new BlockIterator();
		islandGenerator = new IslandGenerator(this);

		// WorldGenerator
		Utils.sendConsoleLog("&7Loading Infiniminer world.");
		worldGenerator = new WorldGenerator();
		Utils.sendConsoleLog("&7World &aloaded!");


		Utils.sendConsoleLog("&aSuccessfully loaded plugin");
	}

	/**
	 * Internal use only
	 */
	@Override
	public void onDisable() {
	}

	@SuppressWarnings("ConstantConditions")
	private void loadCommands() {
		getCommand("infiniminer").setExecutor(new CommandListener(this));

		cmds.put("create", new CreateCmd());
		cmds.put("lobby", new LobbyCmd());

		for (String bc : cmds.keySet()) {
			getServer().getPluginManager().addPermission(new Permission("infiniminer." + bc));
		}
	}

	private void loadEvents() {
		Bukkit.getPluginManager().registerEvents(new JoinEvent(this), this);
	}

}
